#!/usr/bin/env python3

import requests
import json
import yaml

# TODO: import from real sensor without this dummy dict
url = "https://danepubliczne.imgw.pl/api/data/synop/station/olsztyn"
x = requests.get(url)
data=json.loads(x.text)

from_sensor = {
    "sensor" : "olsztyn",
    "temp" : "-3",
    "wetness" : "0.79"
}

from_sensor["temp"] = int(float(data['temperatura']))
from_sensor['wetness'] = round(float(data['wilgotnosc_wzgledna'])/100 , 3)

data=json.dumps(from_sensor)
headers = {'Content-type': 'application/json'}
f = open('./secret.yaml',)
get_secrets = yaml.load(f, Loader=yaml.FullLoader)
auth=(get_secrets["apiuser"], get_secrets["apipass"])

print('Data from sensor:')
print(data)
while True:
    try:
        r = requests.post('http://{}:{}/api/db-write'.format(get_secrets["apihost"], get_secrets["apiport"]), data=data, headers=headers, auth=auth)
        break
    except requests.exceptions.ConnectionError:
        pass
f.close()
print('Response from API')
print(r.text)
