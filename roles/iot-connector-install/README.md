Internet of Things - Connector Ansible Role
=========

It's an Ansible Role to install iot-connector app on Debian/Ubuntu Linux.

Requirements
------------

Add host name from your inventroy to `iot-connector.yaml` file.

Role Variables
--------------

In defaults we have:
- **osuser**: user to use an apllication. **User must exists!**
- **installdir**: where to install an app. Defalult: /opt/iot-connector

In vars dir we must specify: **apiuser**, **apipass**, **apihost** and **apiport**!

Dependencies
------------

None

Example Playbook
----------------

Including an example (`iot-connector.yaml`) with: 

```yaml
---
- hosts: rpi
  roles:
    - role: iot-connector-install
```
Of course you can also specify vars here.

License
-------

BSD

Author Information
------------------

Mateusz Żukowski 2022
