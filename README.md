Internet of Things - Connector 
=========

It's a README file to app itselfe. For README of Ansible Role go to `./roles/iot-connector-install/README.md`

Requirements
------------

System:
- python3.9+

From pip:
- pyyaml
- requests

Installation
--------------

Default way to install an app is using Ansible.

Usage
----------------

Just run an app like:
```bash
/usr/bin/env python3 ./main.py
```

License
-------

BSD

Author Information
------------------

Mateusz Żukowski 2022
